<?php

/**
 * @file
 * Multisite Multiprofile.
 */

use Drupal\config_split\Entity\ConfigSplitEntity;
use Drupal\Core\Site\Settings;
use Symfony\Component\Yaml\Yaml;

/**
 * Implements hook_install_tasks_alter().
 */
function multisite_multiprofile_install_tasks_alter(&$tasks, $install_state) {
  _multisite_multiprofile_install_tasks_alter($tasks, $install_state);
}

/**
 * Add custom tasks for profile installing.
 *
 * @param array $tasks
 *   The tasks to alter.
 * @param array $install_state
 *   The current install state.
 */
function _multisite_multiprofile_install_tasks_alter(array &$tasks, array $install_state) {
  // @codingStandardsIgnoreLine
  global $install_state;

  // Use the Drupal 8.6+ way of importing from existing config.
  if (!$install_state['parameters']['existing_config']) {
    $install_state['parameters']['existing_config'] = TRUE;
  }

  // The installation profile has been removed from core.extension.yml
  // but this config is required for a complete installation.
  if ($install_state['active_task'] == 'install_config_import_batch') {
    $profile = $install_state['parameters']['profile'];
    \Drupal::configFactory()->getEditable('core.extension')
      ->set('profile', $profile)
      ->save();

    // We disabled the translation import to speed up the installation process.
    // It will be automatically enabled again during
    // install_config_import_batch step.
    \Drupal::configFactory()->getEditable('locale.settings')
      ->set('translation.import_enabled', FALSE)
      ->save();
  }

  if (isset($tasks['install_config_import_batch']) && !isset($tasks['install_profile_modules'])) {
    // This tasks is actually doing the opposite of what we are aiming for.
    // It tries to revert to a partial config state.
    unset($tasks['install_config_revert_install_changes']);

    $key = array_search('install_config_import_batch', array_keys($tasks), TRUE);
    $config_tasks = [
      // This one is working around config dependencies likely due to wrong
      // split.
      'install_profile_modules' => [
        'display_name' => t('Install site'),
        'type' => 'batch',
      ],

      // We create the config splits prior the config import.
      '_multisite_multiprofile_update_config_splits' => [
        'display_name' => t('Create config splits'),
        'type' => 'batch',
      ],
    ];
    $tasks = array_slice($tasks, 0, $key, TRUE) +
      $config_tasks +
      array_slice($tasks, $key, NULL, TRUE);
  }
}

/**
 * Updates the config splits from the config/default directory.
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function _multisite_multiprofile_update_config_splits() {
  // Load all config_splits.
  $config_splits = ConfigSplitEntity::loadMultiple();
  $config_factory = \Drupal::configFactory();

  $config_directory = Settings::get('config_sync_directory', FALSE);
  // Get all config_split files.
  $files = preg_grep('~^config_split\.config_split\..*\.yml$~', scandir($config_directory));

  foreach ($config_splits as $config_split) {
    $entity_name = $config_split->getConfigDependencyName();
    $file_path = $config_directory . '/' . $entity_name . '.yml';

    // Update or delete config splits.
    if (file_exists($file_path)) {
      $entity = Yaml::parse(
        file_get_contents($file_path)
      );

      $config_factory->getEditable($entity_name)
        ->setData($entity)
        ->save();

      // Delete the updated file from file list.
      $key = array_search($entity_name . '.yml', $files);
      if ($key) {
        unset($files[$key]);
      }
    }
    else {
      $config_split->delete();
    }
  }

  // The files that hadn't been deleted from the list are new config splits.
  foreach ($files as $file) {
    $file_path = $config_directory . '/' . $file;
    $entity_name = $file = basename($file, ".yml");

    $entity = Yaml::parse(
      file_get_contents($file_path)
    );

    $config_factory->getEditable($entity_name)
      ->setData($entity)
      ->save();
  }

}
