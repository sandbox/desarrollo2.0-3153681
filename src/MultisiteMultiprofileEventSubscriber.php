<?php

namespace Drupal\multisite_multiprofile;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\StorageTransformEvent;

/**
 * Class MultisiteMultiprofileEventSubscriber.
 *
 * @package Drupal\multisite_multiprofile
 */
class MultisiteMultiprofileEventSubscriber implements EventSubscriberInterface {

  /**
   * The installation profile.
   *
   * @var string
   */
  protected $profile;

  /**
   * MultisiteMultiprofileEventSubscriber constructor.
   *
   * @param string $profile
   *   The install profile.
   */
  public function __construct(string $profile) {
    $this->profile = $profile;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::STORAGE_TRANSFORM_IMPORT][] = ['onImportTransform'];
    $events[ConfigEvents::STORAGE_TRANSFORM_EXPORT][] = ['onExportTransform'];
    return $events;
  }

  /**
   * The storage is transformed for importing.
   *
   * @param \Drupal\Core\Config\StorageTransformEvent $event
   *   The config storage transform event.
   */
  public function onImportTransform(StorageTransformEvent $event) {
    $storage = $event->getStorage();
    $extensions = $storage->read('core.extension');
    // Only change something if the sync storage has data.
    if (!empty($extensions)) {
      // Set the current install profile during the config import.
      $extensions['profile'] = $this->profile;
      $storage->write('core.extension', $extensions);
    }
  }

  /**
   * The storage is transformed for exporting.
   *
   * @param \Drupal\Core\Config\StorageTransformEvent $event
   *   The config storage transform event.
   */
  public function onExportTransform(StorageTransformEvent $event) {
    /** @var \Drupal\Core\Config\StorageInterface $storage */
    $storage = $event->getStorage();
    $extensions = $storage->read('core.extension');
    // Prevent the install profile from being exported.
    unset($extensions['profile']);
    // Write to the storage from the event to alter it.
    $storage->write('core.extension', $extensions);
  }

}
